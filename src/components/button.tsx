import * as React from "react"
import classes from "../styles/button.css"

export default class Button extends React.Component {
    render() {
        return (
            <div className={classes.button}>
                {this.props.children}
            </div>
        )
    }
}