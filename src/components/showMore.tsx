import * as React from "react"
import classes from "../styles/showMore.css"
import Button from "./button"

export default class ShowMore extends React.Component {
    render() {
        return(
            <div className={classes.container}>
                <div style={{clear: "both"}}></div>
                {this.props.children}
                <br/>
                <div className={classes["button-container"]}>
                    <Button>
                        عرض المزيد
                    </Button>
                </div>
            </div>
        )
    }
}