import * as React from "react"
import classes from "../styles/section.css"

type props = {
    backgroundColor: string,
    color: string,
    title: string
}

export default class Section extends React.Component<props> {

    render() {
        return(
            <div className={classes.section} style={{backgroundColor: this.props.backgroundColor, color: this.props.color}}>
                <div style={{clear: "both"}} />
                <div className={classes.right}>
                    <div className={classes.title}>
                        {this.props.title}
                    </div>
                    <div className={classes["go-to"]}>
                        <h1 style={{fontSize: "40px", display: "inline"}}>&larr;</h1> تصفح المزيد
                    </div>
                </div>
                <div className={classes.left}>
                    {this.props.children}
                </div>
                <div style={{clear: "both"}} />
            </div>
        )
    }

}