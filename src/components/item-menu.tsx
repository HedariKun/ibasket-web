import * as React from "react"
import classes from "../styles/item-menu.css"
import Button from "./button"
import ItemCard from "./item-card"

function Stars(props) {
    return (
        <div className={classes.stars}>
            <div className="">
                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.15192 0.188477L11.1015 6.19369H17.4105L12.3064 9.90511L14.256 15.9103L9.15192 12.1989L4.04787 15.9103L5.99744 9.90511L0.893387 6.19369H7.20235L9.15192 0.188477Z" fill="#FF1E1E"/>
                        </svg>
                </div>
                <div className="">
                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.15192 0.188477L11.1015 6.19369H17.4105L12.3064 9.90511L14.256 15.9103L9.15192 12.1989L4.04787 15.9103L5.99744 9.90511L0.893387 6.19369H7.20235L9.15192 0.188477Z" fill="#FF1E1E"/>
                        </svg>
                </div>
                <div className="">
                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.15192 0.188477L11.1015 6.19369H17.4105L12.3064 9.90511L14.256 15.9103L9.15192 12.1989L4.04787 15.9103L5.99744 9.90511L0.893387 6.19369H7.20235L9.15192 0.188477Z" fill="#FF1E1E"/>
                        </svg>
                </div>
                <div className="">
                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.15192 0.188477L11.1015 6.19369H17.4105L12.3064 9.90511L14.256 15.9103L9.15192 12.1989L4.04787 15.9103L5.99744 9.90511L0.893387 6.19369H7.20235L9.15192 0.188477Z" fill="#FF1E1E"/>
                        </svg>
                </div>
                <div className="">
                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.15192 0.188477L11.1015 6.19369H17.4105L12.3064 9.90511L14.256 15.9103L9.15192 12.1989L4.04787 15.9103L5.99744 9.90511L0.893387 6.19369H7.20235L9.15192 0.188477Z" fill="#FF1E1E"/>
                        </svg>
                </div>
        </div>
    )
}

export default class ItemMenu extends React.Component {
    render() {
        return (
            <div className={classes.container}>
                <div className={classes["item-container"]}>
                    <div className={classes.image}></div>
                    <div className={classes.body}>
                        <div className="utilies-container">
                            <div className={classes.like}>
                                <div className={classes["like-container"]}>
                                    <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12.6013 0.562744C10.5855 0.562744 9.57125 1.4328 8.56964 2.58026C7.56803 1.4328 6.55382 0.562744 4.53799 0.562744C2.52217 0.562744 0.506348 1.9088 0.506348 5.94383C0.506348 7.96134 2.52217 11.9964 8.56964 15.3599C14.6171 11.9964 16.6329 7.96134 16.6329 5.94383C16.6329 1.9088 14.6171 0.562744 12.6013 0.562744ZM8.56964 13.809C3.07967 10.5494 1.85128 7.05346 1.85128 5.94383C1.85128 4.10285 2.31744 1.9088 4.53799 1.9088C5.94907 1.9088 6.63571 2.41003 7.55858 3.46607L8.56964 4.59777L9.5807 3.46607C10.5036 2.41003 11.1902 1.9088 12.6013 1.9088C14.8218 1.9088 15.288 4.10285 15.288 5.94383C15.288 7.05346 14.0596 10.5494 8.56964 13.809Z" fill="#FF1E1E"/>
                                    </svg>
                                </div>
                            </div>

                            <Stars />

                        </div>
                        <div className={classes.title}>
                        تقوم هنا بكتابة عنوان المنتج المعروض للعملاء
                        </div>
                        <div className={classes.price}>
                            500,000 IQD
                        </div>
                        <div className={classes.description}>
                            × ) ايقفأ ( لسكب 3840 ىلإ لصت ةقدلا ةقئاف ةروصب 4Kلا ةينقتب Server Media 4K beIN زيمتي
                            دقو . ايلاح رفوتملا ةقدلا يلاع HD ـلا ماظن نم لضفأ تارم 4 يه هقدلا هذه ربتعتو ) ايدومع ( طخ 2160
                            ةلضفملا هجمارب ةيئورب دهاشملا عتمتسي يكل هزيمملا ةينقتلا هذه ةفاضإ ىلع beIN تصرح
                            .رخآ لغشم يأ عم رفوتت ال ةدوجلا ةقئاف ةقدو حوضوب
                        </div>
                        <div className={classes["button-container"]}>
                            <Button>اضف الى السلة</Button>
                            <Button>شراء الان</Button>
                        </div>
                        <div className="share"></div>

                    </div>
                </div>
                <div style={{clear: "both"}}></div>
                <div className={classes["similar-product"]}>
                    <div className={classes["similar-title"]}>
                        منتجات مماثلة
                    </div>
                    <div className={classes.items}>

                        <ItemCard />
                        <ItemCard />
                        <ItemCard />
                        <ItemCard />

                    </div>
                </div>
            </div>
        )
    }
}