import * as React from "react"
import * as ReactDom from "react-dom"
import Footer from "./components/footer"
import ItemCard from "./components/item-card"
import TopBar from "./components/top-bar"
import Tags from "./components/tags"
import Promo from "./components/promo"
import Section from "./components/section"
import ShowMore from "./components/showMore"
import ItemMenu from "./components/item-menu"

class Homepage extends React.Component {
    render() {
        return (
            <div>
                <TopBar/>
                <Tags />
                 {/* <Promo />  */}
                {/* <Section backgroundColor="#EFEFEF" color="#5D8854" title="الاكثــــــر مبيعــــــاً">
                    <ItemCard/>
                    <ItemCard/>
                </Section>
                <Section backgroundColor="#5D8854" color="#EFEFEF" title="وصـــــــل حديثــــــاً">
                    <ItemCard/>
                    <ItemCard/>
                    
                </Section>
                
                <ShowMore>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                <ItemCard/>
                </ShowMore> */}
                <ItemMenu/>
                <div style={{clear: "both"}}></div>
                <Footer/>
            </div>
        )
    }
}

ReactDom.render(
    <Homepage />,
    document.getElementById("id")
)